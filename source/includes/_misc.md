# Misc 

The stuff that isn't categorised elsewhere goes here.

## Skin To Win

Originally on Gw2Shinies

There are many items that have the same skin as a more expensive item.  
This tool will find these for you.    


## BLTC Tickets
At its most basic level it is completing a BLTC weapons collection in order to obtain its tickets (normally 7).  
Because they rotate in and out you can often pick up tickets cheaper than what weapons that use them are selling for.  
This allows ye to pick up skins and make a profit at the same time.


**Skin Cost** - category, the cost to complete the collection by either buy orders or instant buy.  
**Ticket Cost** - category, the cost of each ticket with its respective **Skin Cost**.  
**Buy order** - placing an order and waiting for it to fill.  
**Instant Buy** - immediately buying each item from lowest sell listing.  
