# Stats 

Info on my stats pages.

The main purpose of these is to collect and display a wide variety of stats from containers to salvaging to Black Lion Chests.  
Whats on the current list of things to do is to create a summary page.  
This is the way I will be able to port over Breaking Bag, Gold-in-a-box and Shiny Salvage from Gw2Shinies.

## Collection

This tool will capture the droprate data for one item at a time, be it a container, salvage item or Black Lion chest.

### Overview
The core of my stats initiative is the stats collection, it enables accurate data collection for virtually any item.

Data is collected by taking a snapshot of the characters bags and wallet, opening/salvaging items and taking a second snapshot after.  
The difference is then calculated and after basic client side verification it is submitted.  
It is then verified server side that the account and character are real and finally put into the database.


### API Key Security
Worried I will scrape yer API keys?  
Well this is what I use them for: <a href="https://gitlab.com/Silveress_Gw2/Stats_Backend/blob/master/routes/general.js#L16" target="_blank">Source Code</a>.  
And this is where I don't add it to my database: <a href="https://gitlab.com/Silveress_Gw2/Stats_Backend/blob/master/routes/general.js#L67" target="_blank">Source Code</a>.  

### The Cache.
This uses the official API which has a 5 min cache on it which means that results can be up to 5 min old.  
This is not something I can change in the slightest.  
I did petition for an accurate expires header and got that though.  

### Warning

This tool will only capture one process at a time, opening/salvaging multiple items will result in an error.

### How to Use.

1. Enter your API key.
    - This is stored in your browser's cookies.
2.  Enter the basic details, Character, Magic-Find and the activity.
    - If you choose salvaging you will have to select a Salvage Kit as well.
    - You can select the delay dropdown if you want to delay the publication of the results, for profit reasons or whatnot.
        - Note I (as the admin) can see all the hidden sets of data, this is to cut down on abuse.
3. A blue button appears: **Initial Snapshot**, press this to take the before snapshot.
4. Verify that the before snapshot matches you inventory, due to the cache it may not be your current inventory.
    - If the inventory is not correct please wait for the cache expires time to have passed then press **Initial Snapshot** again.
5. Open the container/salvage-item/Black-lion-chest you wish to record.
    - If you open multiple types of containers it will be ruled invalid as the results will be mixed.
    - **Do not delete or consume any of the outputs, that includes luck, junk or ascended junk**
6. Once complete press the **Second Snapshot** button, assuming the initial cache expired it will bring up your new inventory.
7. If there is a difference it will be shown (visually) under the second set of icons.
    - <a href="https://i.imgur.com/D4YSdK2.png" target="_blank">Example of what this looks like.</a>.  
    - If there is no difference wait until the second cache expires and press **Second Snapshot** button again.
8. Assuming there are no errors the blue **Submit** button will appear.
    - If there is an error it will tell you at the bottom of the page.
9. On successful submit the page will reset.
    
    
### Tips
* Do a small scale test first to get an idea of how it works, saves on the frustration of opening 1000 bags and getting no useful data.
* You can open multiple stacks of the same item at the same time, 2000 Trick or Treat bags? Sure if ye have the inventory space.
* You can chain openings one after another (assuming all the items are in your inventory to start with).
    - Do 1-9 above for the first item.
    - For the next item the final cached inventory becomes the starting inventory.
    - Do 1-9 for the second item.
    - Final inventory oif second becomes initial for third.
    - ...


## Viewer

This is where the data from the Collection come together.

* Select the item and the activity, you can type into the field to narrow it down.
* Filters are the same as the <a href="https://slate.silveress.ie/gw2_site#snipe-it" target="_blank">snipe-it ones</a>. 
* Tabs can be collapsed and expanded when clicking on the arrows.
* Live prices are up to 5 min old.
* If an item does not have a market value the vendor value is used.
* The link on the page saves the item, activity, filters and tabs opened/closed for ease of sharing/saving.

An alternate link can take the form of:  
``https://gw2.silveress.ie/stats_viewer?basic=ID,ACTIVITY``    
<a href="https://gw2.silveress.ie/stats_viewer?basic=36038,Opening" target="_blank">https://gw2.silveress.ie/stats_viewer?basic=36038,Opening</a>.   
