# Patreon

I have set up a <a href="https://www.patreon.com/Silver_Golden/overview" target="_blank">Patreon</a> to cover server costs.  
Between hosting and domain names it comes out at ~€13/month, most of which is my database server. (I am using Hetzner by the way, really fantastic host.)
There are two things I have sectioned off, one is a short term benefit, the other long term.

Accounts are updated hourly.  
Names are highly original and utterly unrelated to <a href="https://en.wikipedia.org/wiki/Zero-based_numbering" target="_blank">Zero based indexing</a>.

* Level 0
   * $0/month or $0 total.
   * Can view up to 6 months of data.
   * Updated every 12 Hrs
* Level 1
   * $1/month or $12 total.
   * Can view up to 12 months of data.
   * Updated every 6 hrs.
* Level 2
   * $2/month or $24 total.
   * Can view all data stored.
   * Updated every hour.

What this actually means is that if you only care about the far off history the limit will only come into action three months after you enter your api key (three months of history before the key was entered and three months after)
