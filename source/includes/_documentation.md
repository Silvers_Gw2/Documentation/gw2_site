# Documentation

Just links to the various manuals.

## Website

You are here.

## Gw2 API Parser

My api parser, with some advanced features.  
<a href="https://slate.silveress.ie/gw2_api" target="_blank">Documentation</a>.  

## Stats API

My stats api.  
<a href="https://slate.silveress.ie/gw2_api_stats" target="_blank">Documentation</a>.  
