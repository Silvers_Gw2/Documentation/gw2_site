# Forging 

Info on my forging pages.

## Precursors
 
I am sure that if you search the <a href="https://www.reddit.com/r/Guildwars2/search?q=precursor+forging&sort=top&restrict_sr=on&t=all" target="_blank">subreddit</a>.  you would be able to find plenty datasets of precursor forging.  
However something always niggled at me about them, they are just [precursors out]/[number of forgings] which gives a nice % but nothing of substance beyond that.  

So what makes this different?  
Well for starters I am recording the name and price (at that moment) of every weapon as well as a timestamp (accurate to ~30s).  
This allows me to create other interesting stats as well as creating a psudo forge calculator as well as more advanced stats.  

### Inputs
* Four Exotic Weapons
* No Mystic Forge Stones
* Average level is 76

###Defintions
* No Pres: Average value output per forge excluding precursors
* Just Pres: Average value output per forge excluding non-precursors
* Original: Value of the outputs at time of forging
* Current: Value of the outputs now.

## Precursors Stats

This takes the Precursor data and breaks it down


## Minis
The table layout is much akin to my Precursors page.  
This is the output of forging four rare minis together.  
You will probably notice that the dates are old, I originally started forging minis about two years ago and recorded all the outputs in a spreadsheet.  
Because it is so old it also explains the huge gap between the original values and the current values, minis have plummeted in price over the last year.  

Also much like my precursor page it also doubles as the input for new data but is locked to only me.


## Gemstones

### Craft Gemstones
There is a 10% chance of getting the better output.

Name | Definition
---------- | -------
Name | Name of base material.
Buy order/Instant Buy | How do you buy the materials.
Craft orbs | Use Jeweler to turn crystals into orbs.
Buy orbs | Buy the orbs on order or instant.
10x/1x recipe | Ecto recipe/Dust recipe.
Cost | Price to make one gemstone with the inputs listed. .
Profit| (Sell price * 0.85) - Cost.
Profit % | Profit/Cost.
Unlucky Profit | Worst case output.

### Craft orbs for Sale

Use Jeweler to turn crystals into orbs.
