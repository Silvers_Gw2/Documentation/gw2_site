# Market

This tab deals with everything directly market related, buying selling and whatnot.

## History

I know there are plenty sites that have this done before me, but.  
With this I aim to have the most customisable one as well as having a link to directly what you see.  
Also supporting multiple items.  
And marking of events.  
CSV output is also available

<a href="https://gw2.silveress.ie/history?dates=2015-07-04,2018-10-25&plotBands=livingWorld.LS_S4,expansion&series=b&ids=19721" target="_blank">Example</a>. 

## Snatch-It

My homage to the Gw2Shinies tool of the same name.  


The days value comes from the estimated sales/day over the last month.

1. Buy the lowest listing.
2. Relist
3. ????
4. Profit


## Snipe-it

My homage to the Gw2Shinies tool of the same name


### Filters

* Each filter has the format of "[lower limit],[upper limit]". For example "1,100"
* Gold : 1 = 1g
* Percent: 1 = 1%
* Regular Numbers: 1 = 1

### How the items Max Buy is calculated:

* Price <= Month Max
* Price < Month Average * 2
* Price < Week Average * 3

The days value comes from the estimated sales/day over the last month.

Its not perfect but I am working on it.
Page still takes ages to load as it does quite a few calculations client side.


Rough Instructions:

1. Buy up to the Buy-To price.
2. Relist at Sell price.
3. ????
4. Profit!
