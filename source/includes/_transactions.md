# Transactions


## Backend

So the official api holds 90 days of transactions which works well for most folks.  
However if you have a lot of transactions it can cause most sites to not load up your data as it exceeds the rate limits for the api.

Basically I wanted to fix this.

So how does it work?  
1. So when you submit your details (API key, an email and a password) it then creates and account and adds it to the queue.  
2. Within a minute my server starts pulling your history.
    * While doing this it takes note of what is the latest transaction and records that.
    * This process can take a few minutes for a small history up to 45 min if you are a power trader.
3. It compresses the transactions down into day snapshots.  
3. The next time the process runs it looks for items newer than the last snapshot


This does two things:

* Allows for more than 90 days worth of data.
* Allows people with a large amount of transactions to load up their history.


All the compressed data is accessible from my api, details are typed up <a href="https://slate.silveress.ie/gw2_api#accessing-data" target="_blank">on my api documentation</a>

## Transaction Account
<a href="https://gw2.silveress.ie/transaction_account" target="_blank">Account Page</a>

This page takes a valid api key that has both the "account" and "tradingpost" permissions.  
It also requires you to set a email and password.

When both the key and password have been submitted it gives an estimate of how long it will take to do the first run of the history.  
The page also informs you if you have already set the key/password for that browser.

You can also use this page to change your email/password/apiKey.  
When changing the api key it will only accept a valid key, working on a separate method to delete it.    
To change the password the current email must be used.    
To change the email the current password must be used.    
Its not perfect I know, I will be looking into better ways to do this.

The email is what links it up to patreon so it is recommended you use the same email as that.  

Oh and if you use gmail, patreon does accept aliases such as example+patreon@gmail.com will go to example@gmail.com

## Transaction Viewer
<a href="https://gw2.silveress.ie/transaction_viewer" target="_blank">Viewer</a>

If you haven't set your api key and password this page will redirect back to the account page.  
This page uses the compressed data to show you your transactions.  

The buttons are: 
 
* History type (Buy/Sell)
   * Buy and Sell history.
* Grouping (Full/Compact)
   * Full breaks the output of teh table into days, so if you buy an item on two different days it will be listed as two entries
   * Compact groups all items with the same ID together, so the above example would have one entry.
* Graph (On/Off)
   * Show the graph or not
* CSV
   * This downloads a csv of the data, cant seem to get it to output the compacted data at all (ongoing)
   
### Interaction between Graph and Table
The graph controls the daterange, the table controls the filter.

So if you change the range the graph is focusing on it will change what teh table shows to what fits within that daterange.  
Likewise if you search the table for "ore" the graph will update to only show entries that contain "ore".

### Filters
This table uses the same filters as my <a href="https://slate.silveress.ie/gw2_site#snipe-it" target="_blank">Snipe-it</a> tool


### Plans


* Flipping.
* Comparision to everyone else (suggested).
