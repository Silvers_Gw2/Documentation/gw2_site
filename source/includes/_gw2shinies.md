# Gw2Shinies 

Back in December (2017) RebornGeek said he was closing it down or selling: <a href="https://www.reddit.com/r/Guildwars2/comments/7j77j0" target="_blank">Link</a>.      
July (2018) - New owner interested in moving on: <a href="https://www.reddit.com/r/Guildwars2/comments/8z4wss" target="_blank">Link</a>.  
Even after this the site is up, but highly unstable so I started working on some replacements for the tools.    


## Tools Conversion
Progress on converting the tools.


☑ Amalgamation: First one I created before I knew about above, based off own spreadsheets. <a href="https://gw2.silveress.ie/gemstones" target="_blank">Link</a>.  
☑ Skin to Win: Got this working, somewhat. <a href="https://gw2.silveress.ie/skin_2_win" target="_blank">Link</a>.  
☑ Snatch-it: Complete. <a href="https://gw2.silveress.ie/snatch_it" target="_blank">Link</a>.   
☑ Snipe-it: Complete. <a href="https://gw2.silveress.ie/snipe_it" target="_blank">Link</a>.  
☐ Breaking Bag: In progress.   
☐ Gold-in-a-box: In progress.    
☐ Shiny Salvage: In progress.    
☐ Colors to Dye for: Still no entirely sure what this is.  
☐ Mini Me: Still no entirely sure what this is.  
☐ Smart find: May possibly work on this.  
☒ Calculator: Gw2.ninja has this. <a href="https://gw2.ninja/calc" target="_blank">Link</a>.  
☒ Gem exchange: Gw2spidy has this. <a href="http://www.gw2spidy.com/gem" target="_blank">Link</a>.  
☒ Shard Alchemy: Ew2Efficiency has this. <a href="https://gw2efficiency.com/currencies/spirit-shards" target="_blank">Link</a>.  
