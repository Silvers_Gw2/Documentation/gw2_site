---
title: Silveress's Gw2 Home - Documentation

language_tabs: # must be one of https://git.io/vQNgJ


toc_footers:
  - <a href='https://github.com/lord/slate'>Documentation Powered by Slate</a>

includes:
  - documentation
  - patreon
  - gw2shinies
  - market
  - forging
  - stats
  - misc
  - transactions  

search: true
---

# Introduction

Overview of my Gw2 site, see the left hand side for specific pages.

## Contact

Location | Contact
---------- | -------
Reddit | <a href="https://www.reddit.com/user/silveress_golden/overview" target="_blank">Silveress_Golden</a>.
Discord | @Silver#5563
Guildwars2 | Silveress.5197

## Git repos
<a href="https://gitlab.com/Silveress_Gw2" target="_blank">Main Archive</a>.

In the main archive you will find the documentation folder, the two api's and my main gw2 site.
